<?php
/**
 * Created by PhpStorm.
 * User: ZhMZ
 * Date: 2016/12/31
 * Time: 23:24
 */
include 'common/common.php';
include_once 'common/config.php';

include 'common/filter.php';
include 'common/head.php';
include 'common/navigation.php';
?>
<script language="JavaScript">
    $(function () {
        $('#ContestLi').addClass('nav-current');
    })
</script>
<h1 class="text-center">
    比赛模块正在开发中...
</h1>
    </script>
    <script language="JavaScript">
        $(function () {
            $('#contests-list-table').bootstrapTable({
                //height: $(window).height() - $('.main-header').outerHeight(true) - $('.main-navigation').outerHeight(true) - $('#showClock').outerHeight(true),
                classes: 'table table-striped table-condensed table-hover',
                method: 'get',
                url: 'post/contest/contestListPost.php',
                //search: true,
                columns: [{
                    field: 'ID',
                    title: 'ID',
                    align: 'center',
                    width: '4%',
                }, {
                    field: 'Title',
                    title: 'Contest',
                    align: 'center',
                    width: '35%',
                    searchable: true
                }, {
                    field: 'Author',
                    title: 'Author',
                    align: 'center',
                    width: '14%',
                    searchable: true
                }, {
                    field: 'StartTime',
                    title: 'Start Time',
                    align: 'center',
                    width: '17%'
                }, {
                    field: 'EndTime',
                    title: 'End Time',
                    align: 'center',
                    width: '17%'
                }, {
                    field: 'Type',
                    title: 'Type',
                    align: 'center',
                    width: '7%'
                }, {
                    field: 'State',
                    title: 'State',
                    align: 'center',
                    width: '6%'
                }],
                pagination: true,
                sidePagination: 'server',
                pageSize: 25
            });

            $('#showAccessSubmit').click(function () {
                $.post('contestAccessPost.php', {
                    contest: $('#showAccessContestID').val(),
                    contestAccess: 'submit',
                    contestPassword: $.md5($('#showAccessContestPassword').val()),
                    contestExamUserName: $('#showAccessExamUserName').val(),
                    contestExamPassword: $.md5($('#showAccessExamPassword').val())
                }, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 1) {
                        window.location.href = "contestShow.php";
                    } else {
                        $('#showAccessModel').modal('hide');
                        $('#AlertP').html(result.message);
                        $('#ErrorAlert').modal('show');
                    }
                });
            });
            $('#showAccessReset').click(function () {
                $('#showAccessContestPassword').val('');
                $('#showAccessExamUserName').val('');
                $('#showAccessExamPassword').val('');
            });
        });
    function accessPractice(contest) {
        $.post('contestAccessPost.php', {
            contest: contest,
            contestAccess: 'validate'
        }, function (data) {
            var result = JSON.parse(data);
            if (result.status == 1) {
                window.location.href = "contestShow.php";
            }
            else if(result.status == 2)
            {
                $('#showAccessContestID').val(contest);
                $('#showAccessSubmit').click();
            }
            else
            {
                $('#showAccessModel').modal('hide');
                $('#AlertP').html(result.message);
                $('#ErrorAlert').modal('show');
            }
        });
    }
    function accessContest(contest) {
        $.post('contestAccessPost.php', {
            contest: contest,
            contestAccess: 'validate'
        }, function (data) {
            var result = JSON.parse(data);
            if (result.status == 1) {
                window.location.href = "contestShow.php";
            }
            else if(result.status == 2)
            {
                $('#showAccessContestID').val(contest);
                $('#showAccessContestPassword').val('');
                $('#showAccessExam').removeClass('show').addClass('hidden');
                $('#showAccessContest').removeClass('hidden').addClass('show');
                $('#showAccessModel').modal('toggle');
            }
            else
            {
                $('#showAccessModel').modal('hide');
                $('#AlertP').html(result.message);
                $('#ErrorAlert').modal('show');
            }
        });
    }
    function accessExam(contest) {
        $.post('contestAccessPost.php', {
            contest: contest,
            contestAccess: 'validate'
        }, function (data) {
            var result = JSON.parse(data);
            if (result.status == 1) {
                window.location.href = "contestShow.php";
            }
            else if(result.status == 2)
            {
                $('#showAccessContestID').val(contest);
                $('#showAccessExamUserName').val('');
                $('#showAccessExamPassword').val('');
                $('#showAccessContest').removeClass('show').addClass('hidden');
                $('#showAccessExam').removeClass('hidden').addClass('show');
                $('#showAccessModel').modal('toggle');
            }
            else
            {
                $('#showAccessModel').modal('hide');
                $('#AlertP').html(result.message);
                $('#ErrorAlert').modal('show');
            }
        });
    }
    </script>
    <div class="col-sm-10 col-sm-offset-1 " id="contests-list">
        <div class="row row-margin-bottom" id="showClock" style="margin-top: 3em;margin-bottom: 2em">
            <div class=" col-sm-11">
                <div style="float: left"><span style="font-size: large">System Time : <span id='clock'></span></span></div>
                <div class="btn-group" style="float: right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createContestModel">新建比赛</button>
                    <button type="button" class="btn btn-default">我的比赛</button>
                </div>
            </div>
        </div>
        <table id="contests-list-table">
        </table>
    </div>
    <div class="modal fade" id="createContestModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 1000px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h3 class="modal-title" id="myModalLabel">
                        新建比赛
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="">
                        <div class="">
                            <div class="row row-margin-bottom">
                                <label for="contestCreateContest-title" class="col-sm-2 text-right">标题：</label>
                                <div class="col-sm-9">
                                    <div class="">
                                        <input id="createContest-title" class="form-control" size="16" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-margin-bottom">
                                <label for="contestCreateContest-endTime" class="col-sm-2 text-right">开始时间：</label>
                                <div class="col-sm-9">
                                    <div class="input-group date form_datetime">
                                        <input id="createContest-startTime" class="form-control" size="16" type="text" readonly>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-margin-bottom">
                                <label for="contestCreateContest-endTime" class="col-sm-2 text-right">结束时间：</label>
                                <div class="col-sm-9">
                                    <div class="input-group date form_datetime">
                                        <input id="createContest-endTime" class="form-control" size="16" type="text" readonly>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="">
                            <div class="row row-margin-bottom">
                                <label for="oj" class="col-sm-2 text-right">选择题目：</label>
                                <div class="col-sm-2">
                                    <select class="form-control" id="oj">
                                        <option value="A">A</option>

                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id="oj">
                                        <option value="hdu">HDU</option>
                                        <option value="cf">Codeforces</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <input id="createContest-title" class="form-control" size="16" type="text">
                                </div>
                                <div class="col-sm-1">
                                    <button class="btn btn-primary">添加/修改</button>
                                </div>
                            </div>
                            <div class="row row-margin-bottom">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <table id="createContest-problems">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="showAccessClose" data-dismiss="modal">Close
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="showAccessModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h3 class="modal-title" id="myModalLabel">
                        Access Contest
                    </h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="showAccessContestID">
                    <div id="showAccessContest" class="hidden row-margin-bottom">
                        <div class="row">
                            <label class="col-xs-2 col-sm-offset-3" for="showAccessContestPassword">Password：</label>
                            <div class="col-xs-4">
                                <input type="password" id="showAccessContestPassword" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div id="showAccessExam" class="hidden row-margin-bottom">
                        <div class="row row-margin-bottom">
                            <label class="col-xs-2 col-sm-offset-3" for="showAccessExamUserName">User Name：</label>
                            <div class="col-xs-4">
                                <input type="text" id="showAccessExamUserName" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-xs-2 col-sm-offset-3" for="showAccessExamPassword">Password：</label>
                            <div class="col-xs-4">
                                <input type="password" id="showAccessExamPassword" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row text-center">
                            <div class="col-sm-3 col-sm-offset-3">
                                <input type="button" id="showAccessSubmit" class="form-control" value="Submit" id="showAccessSubmit">
                            </div>
                            <div class="col-sm-3">
                                <input type="reset" id="showAccessReset" class="form-control" value="Reset" id="showAccessReset">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="showAccessClose" data-dismiss="modal">Close
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!--Clock-->
    <script src="common/script/Clock.js" type="text/javascript"></script>
    <link rel='stylesheet' type='text/css' href='common/style/css/bootstrap-datetimepicker.min.css'/>
    <script src='common/script/bootstrap-datetimepicker.min.js' type='text/javascript'></script>
    <input type="hidden" name="Year" id="Year" value="<?php echo date('Y'); ?>">
    <input type="hidden" name="month" id="month" value="<?php echo date('m'); ?>">
    <input type="hidden" name="day" id="day" value="<?php echo date('d'); ?>">
    <input type="hidden" name="Hour" id="Hour" value="<?php echo date('H'); ?>">
    <input type="hidden" name="minute" id="minute" value="<?php echo date('i'); ?>">
    <input type="hidden" name="second" id="second" value="<?php echo date('s'); ?>">
    <script>
        $(
            $("#createContest-startTime").datetimepicker({
                format: "yyyy-mm-dd hh:ii:00",
                weekStart: 1,
                todayBtn: 1,
                autoclose: true,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                initialDate: new Date()
            }),
            $("#createContest-endTime").datetimepicker({
                format: "yyyy-mm-dd hh:ii:00",
                weekStart: 1,
                todayBtn: 1,
                autoclose: true,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                initialDate: new Date()
            }),
            $('#createContest-problems').bootstrapTable({
                //height: $(window).height() - $('.main-header').outerHeight(true) - $('.main-navigation').outerHeight(true) - $('#showClock').outerHeight(true),
                classes: 'table table-striped table-condensed table-hover',
                method: 'get',
                url: 'post/contest/contestListPost.php',
                //search: true,
                columns: [{
                    field: 'ID',
                    title: 'ID',
                    align: 'center',
                    width: '4%',
                }, {
                    field: 'Title',
                    title: 'Contest',
                    align: 'center',
                    width: '35%',
                    searchable: true
                }, {
                    field: 'Author',
                    title: 'Author',
                    align: 'center',
                    width: '14%',
                    searchable: true
                }, {
                    field: 'StartTime',
                    title: 'Start Time',
                    align: 'center',
                    width: '17%'
                }, {
                    field: 'EndTime',
                    title: 'End Time',
                    align: 'center',
                    width: '17%'
                }, {
                    field: 'Type',
                    title: 'Type',
                    align: 'center',
                    width: '7%'
                }, {
                    field: 'State',
                    title: 'State',
                    align: 'center',
                    width: '6%'
                }],
                pagination: true,
                sidePagination: 'server',
                pageSize: 25
            })
        )

        var serverDate = new Date(document.getElementById('Year').value, document.getElementById('month').value, document.getElementById('day').value, document.getElementById('Hour').value, document.getElementById('minute').value, document.getElementById('second').value);
        var clientDate = new Date();
        var expiresSecond = serverDate - clientDate;
        var clock = new Clock();
        clock.display(document.getElementById("clock"));
    </script>
    <!--Clock end-->

<?php
include 'common/footer.php';