<?php
/**
 * Created by PhpStorm.
 * User: ZhMZ
 * Date: 2016/10/5
 * Time: 10:33
 */
include 'common/common.php';
include_once 'common/config.php';
$config["needRoot"] = true;
include 'common/filter.php';
include 'common/head.php';
include 'common/navigation.php';

$subject = "abcdef";
$pattern = '/deffs$/';
if(preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE, 3)){
    print_r($matches);
};

?>
<h1 class="text-center">系统日志</h1>
<hr/>
<div class="row row-margin-bottom col-sm-8 col-sm-offset-2">
    <table id="logList">
    </table>
</div>


<div class="modal fade" id="showLogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title" id="myModalLabel">
                    System Log
                </h3>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <button class="btn btn-default" type="button" onclick="refreshDetails()" title="Refresh"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
                        </div>
                    </div>
                    <div class="panel-body">
                        <xmp id="showLogDetails-content" style="color:red"></xmp>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="showCEClose" data-dismiss="modal">Close
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<script>
var log_id = 0;
function showDetails(id) {
    log_id = id;
    $.post(
        'post/admin/logListDetails.php',
        {
            id:id
        },
        function(data) {
            $('#showLogDetails-content').html(data);
            $('#showLogDetails').modal('toggle');
        }
    )
}
function deleteLog(id) {
    log_id = id;
    $.post(
        'post/admin/logDeletePost.php',
        {
            id:id
        },
        function(data) {
            if(data=='success'){
                $('#logList').bootstrapTable('refresh');
            }
            else {
                $('#AlertP').html(data);
                $('#ErrorAlert').modal('toggle');
            }
        }
    )
}
function refreshDetails() {
    $.post(
        'post/admin/logListDetails.php',
        {
            id:log_id
        },
        function(data){
            $('#showLogDetails-content').html(data);
        }
    )
}
$(
    $('#logList').bootstrapTable({
        classes: 'table table-striped table-hover',
        method: 'get',
        url: 'post/admin/logListPost.php',
        showRefresh: true,
        //search:true,
        columns: [{
            field: 'id',
            title: 'ID',
            align: 'center',
            width: '10%'
        }, {
            field: 'time',
            title: 'Time',
            align: 'center',
            width: '30%'
        }, {
            field: 'detail',
            title: '查看详细',
            align: 'center',
            width: '30%'
        }, {
            field: 'delete',
            title: '删除',
            align: 'center',
            width: '30%'
        }],
        pagination: true,
        sidePagination:'server',
        pageSize: 25
    })

);
</script>

