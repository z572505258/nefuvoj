<?php
/**
 * Created by PhpStorm.
 * User: HuangBook
 * Date: 2016/2/19
 * Time: 20:27
 */
include '../common/common.php';
include_once '../common/config.php';

include '../common/filter.php';

$showCount = $con->safeGetRequest("limit",true);
$currentOffset = $con->safeGetRequest("offset",true);
$sOj = $con->safeGetRequest("oj");
$sPid = $con->safeGetRequest("pid");
$sUid = $con->safeGetRequest("uid");
$sResult = $con->safeGetRequest("result");

$maxSolutionID =200;
$query="SELECT count(*) AS maxSolutionID  FROM v_solutions WHERE contest_id=0";
if ($sPid != null && $sPid != "") {
    $sPid = strtoupper($sPid);
    $query = $query . " and CONCAT(oj,problem_id) = '$sPid'";
}
if ($sUid != null && $sUid!="") {
    $query = $query . " and user_login_id='" . $sUid . "'";
}
if ($sResult != null && $sResult!="") {
    $query = $query . " and status like '$sResult%'";
}
if( ($sPid != null && $sPid != "") || ($sUid != null && $sUid!="") || ($sResult != null && $sResult!="")) {
    $result = $con->exeSql($query);
    $row = mysqli_fetch_array($result, MYSQLI_BOTH);
//当前可以显示的最大题目id
    $maxSolutionID = $row['maxSolutionID'];
}
else {
    //$query="SELECT max(solution_id) AS maxID  FROM solution WHERE contest_id=0 and solution_id > 214000";
    $query = "SELECT AUTO_INCREMENT as maxID FROM information_schema.TABLES WHERE TABLE_SCHEMA = \"".$con->database."\" AND TABLE_NAME = \"voj_solution\"";
    $result = $con->exeSql($query);
    $row = mysqli_fetch_array($result, MYSQLI_BOTH);
    $line = $row['maxID'] - 1000;
}



//$query = "SELECT * FROM solution WHERE (solution.contest_id in (SELECT contest.contest_id FROM contest WHERE contest.type<>'EXAM' AND contest.end_time < now()) OR solution.contest_id=0)";
$query="SELECT * FROM v_solutions where 1 ";
if ($sPid != null && $sPid != "") {
    $query = $query . " and CONCAT(oj,problem_id) = '$sPid'";
}
if ($sUid != null && $sUid != "") {
    $query = $query . " and user_login_id='" . $sUid . "'";
}
if ($sResult != null && $sResult != "") {
    $query = $query . " and status like '$sResult%'";
}
if(($sPid == null || $sPid == "") && ($sUid == null && $sUid=="") && ($sResult == null && $sResult=="") ) {
    $query .= " and id > ".$line;
}
$query = $query . " ORDER BY id DESC LIMIT " . $currentOffset . "," . $showCount . ";";
$result = $con->exeSql($query);
$statusList = array();
$privilege= isset($_SESSION['v_privilege']) ? $_SESSION['v_privilege'] : -1;

while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
    $obj = array(
        'solution_id' => $row['id'],
        'user_name' =>"<a href=\"javascript:void(0);\" onclick='showUserInformation(\"".$row['user_login_id']."\")'>" . $row['user_login_id'] . "</a>" ,
        'nick_name' => $row['user_name'],
        'problem_id' => "<a href=\"problemShow.php?oj=".$row["oj"]."&pid=".$row["problem_id"]."\">".strtoupper($row["oj"]).$row['problem_id']."</a>",
        'result' => "<span class=\"status status".$row["status_code"]."\">".$row["status"]."</span>",
        'memory' => $row['memory'],
        'time' => $row['time'],
        'language' => $row["language_show"],
        'code_length' => $row['code_length']."B",
        'in_date' => $row['in_date']
    );
    if($row["status_code"]==2) {
        $obj["result"]="<span class=\"status\"><a href=\"javascript:void(0);\" onclick='showCE(".$row["id"].")'>".$row["status"]."</a></span>";
    }
    switch($privilege) {
        case 1://一级管理员
            $obj['code_length'] ="<a href=\"javascript:void(0);\" onclick='showSC(".$row['id'].")'>" . $row['code_length'] . "B</a>";
            break;
        case -1://没有登录
            $obj['code_length'] =$row['code_length']."B";
            break;
        default:
            if (strcmp($_SESSION['v_login_id'], $row['user_login_id']) == 0) {//自己的代码
                $obj['code_length'] ="<a href=\"javascript:void(0);\" onclick='showSC(".$row['id'].")'>" . $row['code_length'] . "B</a>";
            }
    }
    array_push($statusList, $obj);
}
$data = array(
    'total' => $maxSolutionID,
    'rows' => $statusList
);
echo json_encode($data);