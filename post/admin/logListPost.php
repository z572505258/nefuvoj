<?php
/**
 * Created by PhpStorm.
 * User: ZhMZ
 * Date: 2016/12/30
 * Time: 12:47
 */
include '../../common/common.php';
include_once '../../common/config.php';
$config['needRoot'] = true;
include '../../common/filter.php';
$showCount = $con->safeGetRequest("limit",true);
$currentOffset = $con->safeGetRequest("offset",true);

$query = "select count(*)  from v_log where 1 ";
$result = $con->exeSql($query);
$row=mysqli_fetch_array($result,MYSQLI_BOTH);
$tot = $row[0];

$query = "select id,time  from v_log where 1 ";
$query.="ORDER BY id desc LIMIT $currentOffset ,$showCount";
$resultT = $con->exeSql($query);

$rows = array();
while($rowT=mysqli_fetch_array($resultT,MYSQLI_BOTH)) {
    $id = $rowT['id'];
    $time = $rowT['time'];
    $row = array(
        "id"=>$id,
        "time"=>$time,
        "detail"=>"<a onclick='showDetails($id)'>显示详细信息</a>",
        "delete"=>"<a onclick='deleteLog($id)'>删除</a>"
    );
    array_push($rows,$row);
}
$obj = array(
    "total"=>$tot,
    "rows"=>$rows
);
echo json_encode($obj);