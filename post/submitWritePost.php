<?php
/**
 * Created by PhpStorm.
 * User: YELLOW2013
 * Date: 2016/2/28 0028
 * Time: 2:48
 */
include '../common/common.php';
include_once '../common/config.php';
$config['needLogin'] = true;
include '../common/filter.php';

$oj = $con->safeGetRequest("oj");
//if($oj!='hdu') returnError("无法提交，参数错误");
$pid = $con->safeGetRequest("pid");
$code = isset($_REQUEST['code'])?$_REQUEST['code']:'';
$language = $con->safeGetRequest("language");
//$code = $con->safeGetRequest("code");
$obj = array(
    'status' => 0,
    'message' => ""
);
function GetIP()
{
    if (!empty($_SERVER["HTTP_CLIENT_IP"]))
        $cip = $_SERVER["HTTP_CLIENT_IP"];
    else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
        $cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
    else if (!empty($_SERVER["REMOTE_ADDR"]))
        $cip = $_SERVER["REMOTE_ADDR"];
    else
        $cip = "0.0.0.0";
    return $cip;
}

if (strlen($code) < 0x1c9c380) {
    $ip = GetIP();
    $now = date('Y-m-d H:i:s');
    $codeLength = strlen($code);
    $code = $con->safeGetRequest("code");
    $language_show = "";
    $query = "select `show` from v_language where oj='$oj' and value='$language'";
    $result = $con->exeSql($query);
    if($row = mysqli_fetch_array($result,MYSQLI_BOTH)) $language_show = $row["show"];

    $query = "INSERT INTO 
               v_solutions(
                    user_login_id,
                    user_name,
                    problem_id,
                    oj,
                    in_date,
                    status_flag,
                    status,
                    status_code,
                    language,
                    language_show,
                    ip,
                    code,
                    code_length
              ) VALUES(
                  '".$_SESSION['v_login_id']."',
                  '".$_SESSION['v_user_name']."',
                  '".$pid."',
                  '$oj',
                  '".$now."',
                  0,
                  'Waiting',
                  0,
                  '".$language."',
                  '".$language_show."',
                  '".$ip."',
                  '".$code."',
                  '".$codeLength."'
              )";

    $result = $con->exeSql($query);

    $obj["status"] = 1;
    $obj["message"] = "提交成功!<script>setTimeout('window.location.href=\"status.php\"',800);</script>";
} else {
    $obj["status"] = 0;
    $obj["message"] = " Code is too long!";
}

echo json_encode($obj);