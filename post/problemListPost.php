<?php
/**
 * Created by PhpStorm.
 * User: ZhMZ
 * Date: 2016/10/5
 * Time: 9:12
 */
include '../common/common.php';
include_once '../common/config.php';

include '../common/filter.php';

$showCount = $con->safeGetRequest("limit",true);
$currentOffset = $con->safeGetRequest("offset",true);
$oj = $con->safeGetRequest("oj");
$searchText = $con->safeGetRequest("searchText");

$query = "select count(*)  from v_problems where 1 ";
if($oj!=null && $oj!="all") $query.=" and oj='$oj'";
if($searchText!=null) $query.=" and (title like '%$searchText%' OR problem_id like '$searchText%')";
$result = $con->exeSql($query);
$row=mysqli_fetch_array($result,MYSQLI_BOTH);
$tot = $row[0];

$query = "select id,oj,title,problem_id,update_time  from v_problems where 1 ";
if($oj!=null && $oj!="all") $query.=" and oj='$oj'";
if($searchText!=null) $query.=" and (title like '%$searchText%' OR problem_id like '$searchText%')";
$query.="ORDER BY id LIMIT $currentOffset ,$showCount";
$resultT = $con->exeSql($query);

$rows = array();
while($rowT=mysqli_fetch_array($resultT,MYSQLI_BOTH)) {
    $oj = $rowT["oj"];
    $pid = $rowT["problem_id"];
    $title = $rowT["title"];
    $updtime = $rowT["update_time"];
    $status = 0;
    if(isset($_SESSION['v_login_id'])) {
        $query = "select count(*) from v_solutions where user_login_id='".$_SESSION['v_login_id']."' and oj='$oj' and problem_id='$pid' and status_flag=3 and contest_id=0";
        $result = $con->exeSql($query);
        $row=mysqli_fetch_array($result,MYSQLI_BOTH);
        if($row[0]!=0) {
            $query = "select count(*) from v_solutions where user_login_id='".$_SESSION['v_login_id']."' and oj='$oj' and problem_id='$pid' and status_code=1 and contest_id=0";
            $result = $con->exeSql($query);
            $row=mysqli_fetch_array($result,MYSQLI_BOTH);
            if($row[0]!=0) {
                $status = 1;
            }
            else $status = 2;
        }
    }


    $row = array(
        "oj"=>strtoupper($oj),
        "status"=>$status,
        //"pid"=> "<a href='problemShow.php?oj=".$oj."&pid=".$pid."' target='_blank'>".strtoupper($oj).$pid."</a>",
        "pid"=> "<a href='problemShow.php?oj=".$oj."&pid=".$pid."' target='_blank'>".$pid."</a>",
        "title"=>"<a href='problemShow.php?oj=".$oj."&pid=".$pid."' target='_blank'>".$title."</a>",
        "updtime"=>$updtime
    );
    array_push($rows,$row);
}
$obj = array(
    "total"=>$tot,
    "rows"=>$rows
);
echo json_encode($obj);