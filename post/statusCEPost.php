<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/2/24
 * Time: 15:53
 */

include '../common/common.php';
include_once '../common/config.php';

include '../common/filter.php';

$obj = array(
    'status' => 0,
    'message' => ""
);

$solution_id = isset($_REQUEST['solution_id']) ? $_REQUEST['solution_id'] : 0;
if($solution_id!=0){
    $query = "SELECT *  FROM v_solutions WHERE id=".$solution_id;
    $result = $con->exeSql($query);
    $row = mysqli_fetch_array($result, MYSQLI_BOTH);
    $obj["data"]=array(
        "Problem"=>$row["oj"].$row["problem_id"],
        //"Author"=>$row["user_id"],
        "JudgeStatus"=>"<span class='status'>".$row["status"]."</span>",
        "CEInformation"=>$row["CE_info"]
    );
    $obj["status"]=1;
}else{
    $obj["status"] = 0;
    $obj["message"] = "参数错误！";
}
echo json_encode($obj);