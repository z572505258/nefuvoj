<?php
/**
 * Created by PhpStorm.
 * User: YELLOW2013
 * Date: 2016/2/28 0028
 * Time: 22:30
 */
include '../../common/common.php';
include_once '../../common/config.php';

include '../../common/filter.php';

$showCount = $con->safeGetRequest('limit',true);
$currentOffset = $con->safeGetRequest('offset',true);

$query = "select count(*) from v_contest";
$result = $con->exeSql($query);
$row = mysqli_fetch_array($result, MYSQLI_BOTH);
$ContestCount = $row['count(*)'];

$query = "select * from v_contest ORDER BY id DESC LIMIT " . $currentOffset . "," . $showCount . ";";
$result = $con->exeSql($query);
$contestsList = array();
$now=date("Y-m-d H:i:s");
$nickNames = array();
while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
    $obj = array(
        'ID' => $row['id'],
        'Title' =>"<a onclick='accessContest(".$row["id"].")'>".$row["title"]."</a>",
        'Type' => '',
        'StartTime' => $row['start_time'],
        'EndTime' => $row['end_time'],
        //'Author' => $row['user_name'],
        'Author' => 'null',
    );
    if(!isset($nickNames[$row['manager_id']])) {
        $queryNickname = "select user_name from v_users WHERE id='" . $row["manager_id"] . "'";
        $resultNickname = $con->exeSql($queryNickname);
        $rowNickname = mysqli_fetch_array($resultNickname, MYSQLI_BOTH);
        $nickNames[$row['manager_id']]=$rowNickname["user_name"];
    }
    $obj["Author"]=$nickNames[$row['manager_id']];


    if($now<$row['start_time']) {
        $obj['State']="Prepare";
    }else if($now > $row['end_time']){
        $obj['State']="End";
    } else{
        $obj['State']="Running";
    }
    array_push($contestsList, $obj);
}
$data = array(
    'total' => $ContestCount,
    'rows' => $contestsList
);
echo json_encode($data);