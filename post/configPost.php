<?php
/**
 * Created by PhpStorm.
 * User: 15545
 * Date: 2016/9/30
 * Time: 20:19
 */

include '../common/common.php';
include_once '../common/config.php';

include '../common/filter.php';

$op = $con->safeGetRequest("op");

$obj = array(
    "status"=>0,
    "message"=>""
);

if($op=="daemon_close_flag") {
    $query = "update admin_config set close_daemon=if(close_daemon=1,0,1) where id = 1";
    $result = $con->exeSql($query);
    $obj["status"] = 1;
    echo json_encode($obj);
}
else if($op=="startDaemon") {
    //exec('php E:\server\www\nefuvoj\daemon\daemon.php');
    $obj["status"] = 1;
    echo json_encode($obj);
}
