<?php
/**
 * Created by PhpStorm.
 * User: ZhMZ
 * Date: 2016/10/4
 * Time: 22:23
 */
include '../common/common.php';
include_once '../common/config.php';

include '../common/filter.php';

$obj = array(
    'status' => 0,
    'message' => "未知错误"
);
$order = $con->safeGetRequest('order',true);


if ($order == 1) {
    $userName = $con->safeGetRequest('userName');
    $password = $con->safeGetRequest('password');
    $password = md5("developer_ZhMZ_from_nefu_qq_572505258".$password);
    $remember = $con->safeGetRequest('remember');
    if($userName!=""&&$password!="") {
        $query = "SELECT * FROM v_users WHERE BINARY login_id='" . $userName . "'";
        $result = $con->exeSql($query);
        if ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            if (strcmp($row["password"], $password) == 0) {
                session_destroy();
                session_start();
                //$_SESSION["v_id"] = $row["id"];
                $_SESSION["v_login_id"] = $userName;
                $_SESSION["v_user_name"] = $row["user_name"];
                $_SESSION["v_login"] = "1";
                $_SESSION["v_privilege"] = $row["privilege"];
                $date = date('Y-m-d H:i:s');
                $query = "UPDATE v_users SET last_login_time='" . $date . "' WHERE id='" . $row['id'] . "'";
                $result = $con->exeSql($query);
                if($remember=="true") {
                    //先判断数据库中是否保存了某个remember信息
                    $existence = false;
                    $date = date('Y-m-d H:i:s');
                    $query = "SELECT cookie_key from v_users where `login_id` = '$userName' and cookie_timeline > '$date' and !isnull(cookie_key)";
                    $result = $con->exeSql($query);
                    if($row=mysqli_fetch_array($result, MYSQLI_BOTH)) {
                        if(strlen($row["cookie_key"])==32) {
                            $existence = true;
                            $key = $row["cookie_key"];
                        }
                    }
                    if($existence == false) {
                        $str = "3456701289abcdUVWXYZmnpefPQRSTqrstughijkNvwxyzABCDEFGHIJKLM";
                        $key = '';
                        for ($i = 0; $i < 32; $i++) {
                            $key .= $str[mt_rand(0, strlen($str) - 1)];
                        }
                    }
                    $timeline = date('Y-m-d H:i:s',strtotime('+7 day'));
                    $query = "UPDATE `v_users` SET `cookie_key`='$key', `cookie_timeline`='$timeline' WHERE `login_id`='$userName';";
                    $result = $con->exeSql($query);
                    setcookie("userName", $userName, time()+3600*24*7);
                    setcookie("userKey", $key , time()+3600*24*7);
                }
                $obj["status"] = 1;
            } else {
                $obj["status"] = 0;
                $obj["message"] = "密码不正确！";
            }
        }else{
            $obj["status"] = 0;
            $obj["message"] = "用户名不存在！";
        }
    }
    else{
        $obj["status"] = 0;
        $obj["message"] = "参数错误2！";
    }
} else if ($order == 2) {
    session_destroy();
    setcookie("userName", "", time()-3600);
    setcookie("userKey", "" , time()-3600);
    $obj["status"] = 1;
}
else {
    $obj["message"] = "参数错误3！";
}
echo json_encode($obj);