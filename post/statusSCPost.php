<?php
/**
 * Created by PhpStorm.
 * User: ZhMZ
 * Date: 2016/10/4
 * Time: 23:34
 */
include '../common/common.php';
include_once '../common/config.php';
$config['needLogin'] = true;
include '../common/filter.php';

$obj = array(
    'status' => 0,
    'message' => ""
);


$solution_id = $con->safeGetRequest('solution_id');

$userId = isset($_SESSION["v_login_id"]) ? $_SESSION["v_login_id"] : "";
$privilege = isset($_SESSION["v_privilege"]) ? $_SESSION["v_privilege"] : 0;
if($solution_id!=0 && $userId!="") {
    $query = "SELECT *  FROM v_solutions where id=".$solution_id." and contest_id=0";
    $result = $con->exeSql($query);
    $row = mysqli_fetch_array($result, MYSQLI_BOTH);
    if($row==null) {
        $obj["status"] = 0;
        $obj["message"] = "记录不存在！";
        echo json_encode($obj);
        exit();
    }
    if($row['user_login_id']!=$userId && $privilege!=1) {  //非一级管理员不能看别人代码，只能看自己代码
        $obj["status"] = 0;
        $obj["message"] = "没有权限！";
        echo json_encode($obj);
        exit();
    }
    $obj["data"]=array(
        "Problem"=>$row["problem_id"],
        "Author"=>$row["user_name"],
        "JudgeStatusCode"=>$row["status"],
        "JudgeStatus"=>"<span class=\"status status".$row["status_code"]."\">".$row["status"]."</span>",
        "SCCode"=>$row["code"],
        "CEInformation"=>$row["CE_info"]
    );
    $obj["status"]=1;
}else{
    $obj["status"] = 0;
    $obj["message"] = "参数错误！";
}

echo json_encode($obj);