<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/8/15
 * Time: 11:15
 */

class connect {
    private $server = "localhost";
    private $username = "root";
    private $localPassword = "123456";
    public $database = "nefu_voj2016";

    private $link;
    function __construct()
    {
        $this->link = mysqli_connect($this->server, $this->username, $this->localPassword, $this->database) or die("Connect failed1: " . mysqli_connect_error() . "\n");
        mysqli_query($this->link, "set names utf8") or die("Connect failed2: " . mysqli_connect_error() . "\n");
        mysqli_query($this->link, "SET CHARACTER SET utf8") or die("Connect failed3: " . mysqli_connect_error() . "\n");
        //mysqli_query($link, "SET COLLATION_CONNECTION='utf8'") or die("Connect failed4: " . mysqli_connect_error() . "\n");
    }
    function &exeSql($query,$errorFlag="") {
        $result = mysqli_query($this->link, $query) or die($query."<br>Error Message:".$errorFlag . mysqli_error($this->link) . "\n");
        return $result;
    }

    function safeGetRequest($valName,$isNumber=false) {
        if(isset($_REQUEST[$valName])) {
            if($isNumber) {
                if (!is_numeric($_REQUEST[$valName])) return null;
            }
            return mysqli_escape_string($this->link,$_REQUEST[$valName]);
        }
        else return null;
        //else return $isNumber ? 0:"";
    }
}
$con = new connect();
