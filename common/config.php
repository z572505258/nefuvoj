<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/2/2
 * Time: 15:14
 */


$config_default = array(
    //当前页面禁用
    "forbidden" => false,
    //允许从url直接访问
    "needEmptyRefer" => false,
    //需要登录比赛
    "needContestSession" => false,
    //需要管理员权限
    "needRoot" => false,
    //需要登录
    "needAdmin" => false,
    //错误展示(0:不展示，不跳转；1：不展示，跳转；2：展示，跳转)
    "showMessage" => 0,
);
