<?php
/**
 * Created by PhpStorm.
 * User: YELLOW2013
 * Date: 2016/1/24 0024
 * Time: 23:49
 */
?>
<!DOCTYPE html>
<head>
    <?php
    header('Cache-Control: no-cache, must-revalidate');
    header('Pragma:no-cache');
    ?>
    <meta name=keywords content='nefu,NEFU,acm,ACM,ICPC,oj,OJ,virtual judge,voj'>
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
    <title>NEFU Virtual Judge</title>
</head>
<body>
<?php
require_once 'css+js.html';
?>
<!--alert bootstrap-->
<div class="modal fade" id="ErrorAlert" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Alert!
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <strong><p id="AlertP"></p></strong>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="ErrorAlertClose"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
