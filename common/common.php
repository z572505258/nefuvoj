<?php
/**
 * Created by PhpStorm.
 * User: 15545
 * Date: 2016/8/4
 * Time: 18:48
 */
define("APP_ROOT",dirname(__FILE__));
$config = array(
    //需要管理员权限//不需要
    "needRoot" => false,
    //需要登录 //不需要
    "needLogin" => false,
);
session_start();
header('Content-Type: text/html; charset=utf-8');

//require_once 'common/Mapping.php';
//require_once 'common/Function.php';
require_once 'connect.php';
//require_once 'common/config.php';

$obj = array(
    "status" => 0,
    "message" => ""
);
function returnError($mes) {
    $obj = array(
        "status" => 0,
        "message" => $mes
    );
    echo json_encode($obj);
    exit();
}
function returnPageError($mes="页面出错啦") {
    echo "
        <div class='row-margin-bottom'>
            <h1 class='text-primary text-center'>$mes</h1>
        </div>
        <div class='row-margin-top'>
            <img src=\"common/Image/littleYellowMan.jpg\" class='img-rounded img-circle img-thumbnail center-block'>
        </div>
        
    ";
    exit();
}