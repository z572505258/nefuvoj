<?php
/**
 * Created by PhpStorm.
 * User: 15545
 * Date: 2016/8/4
 * Time: 18:49
 */
$MESSAGE = array(
    "status" => 0,
    "message" => ''
);

if ($config["needLogin"]) {
    if (!isset($_SESSION["v_login_id"])) {
        returnError("你还没有登录！或者刷新重试。");
        exit(0);
    }
}

if ($config["needRoot"]) {
    if (!isset($_SESSION["v_login"]) || $_SESSION["v_privilege"] != 1) {
        returnError("请登录管理员账号哦");
        exit(0);
    }
}


