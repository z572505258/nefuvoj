<?php
/**
 * Created by PhpStorm.
 * User: ZhMZ
 * Date: 2016/9/29
 * Time: 22:29
 */
include 'common/common.php';
include_once 'common/config.php';

include 'common/filter.php';
include 'common/head.php';
include 'common/navigation.php';


$oj = isset($_COOKIE["problem_oj"])?$_COOKIE["problem_oj"]:'all';
$searchText = isset($_COOKIE["problem_searchText"])?$_COOKIE["problem_searchText"]:'';

?>
<script language="JavaScript">
    $(function () {
        $('#ProblemLi').addClass('nav-current');
    })
</script>
<div class="container">
    <div class="panel panel-info">
        <div class="panel-heading">
        </div>
        <div class="panel-body">
        </div>
        <div class="row row-margin-bottom">
            <div class="col-sm-2">
                <div class="row">
                    <label for="oj" class="col-xs-2"></label>
                    <div class="col-xs-10">
                        <select class="form-control" id="oj">
                            <option value="all" <?=$oj=='all'?'selected="true"':'' ?>>All</option>
                            <option value="hdu" <?=$oj=='hdu'?'selected="true"':'' ?>>HDU</option>
                            <option value="cf" <?=$oj=='cf'?'selected="true"':'' ?>>Codeforces</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" id="search-text" placeholder="search for Title/ProblemID" <?=$searchText!=''?'value='.$searchText.'':'' ?>>
                    </div>
                </div>
            </div>
        </div>
        <table id="problem-list">
        </table>
    </div>
</div>
<script>
    $(function () {
        $('#problem-list').bootstrapTable({
            //height: $(window).height() - $('.main-header').outerHeight(true) - $('.main-navigation').outerHeight(true) - $('#status-search-div').outerHeight(true),
            //height: pageSize,
            classes: 'table table-no-bordered table-hover',
            method: 'get',
            url: 'post/problemListPost.php',
            //search: true,
            columns: [
                {
                    field: 'status',
                    title: 'Status',
                    align: 'center',
                    width: '5%',
                    formatter: function (value, row, index) {
                        switch (row.status) {
                            case 0:
                                return "<span class=''></span>";
                                break;
                            case 1:
                                return "<span class='glyphicon glyphicon-ok'></span>";
                                break;
                            case 2:
                                return "<span class='glyphicon glyphicon-remove'></span>";
                                break;
                        }
                        return value;
                    },

                    cellStyle: function (value, row, index) {
                        switch (row.status) {
                            case 0:
                                return {classes: '', css: {}};
                                break;
                            case 1:
                                return {classes: 'success', css: {}};
                                break;
                            case 2:
                                return {classes: 'danger', css: {}};
                                break;
                        }
                        return {};
                    }


                },
                {
                    field: 'oj',
                    title: 'OJ',
                    align: 'center',
                    width: '10%',
                },
                {
                    field: 'pid',
                    title: 'Problem ID',
                    align: 'center',
                    width: '10%',
                },
                {
                    field: 'title',
                    title: 'Title',
                    align: 'center',
                    width: '50%',
                },
                {
                    field: 'tag',
                    title: 'Tag',
                    align: 'center',
                    width: '5%',
                },
                {
                    field: 'updtime',
                    title: 'Updata Time',
                    align: 'center',
                    width: '20%',
                }
                ],
            pagination: true,
            sidePagination: 'server',
            pageSize: 25,
            queryParams: function (params) {
                return {
                    limit: params.limit,
                    offset: params.offset,
                    oj: $('#oj').val(),
                    searchText: $('#search-text').val()
                };
            },
        });
        $('#oj').change(function() {
            go();
        });
        $("#search-text").on('input',function(e){
            go();
        });

    });
    function go() {
        document.cookie= "problem_oj="+$('#oj').val(),+";";
        document.cookie= "problem_searchText="+$('#search-text').val(),+";";
        $('#problem-list').bootstrapTable('refresh');
    }
</script>
<?php
include "common/footer.php";
