<?php
/**
 * Created by PhpStorm.
 * User: ZhMZ
 * Date: 2016/9/29
 * Time: 22:36
 */
require_once 'common/common.php';
require_once 'common/config.php';

require_once 'common/filter.php';
require_once 'common/head.php';
require_once 'common/navigation.php';

require_once 'crawler/hduProblem.php';
require_once 'crawler/cfProblem.php';

$oj = $con->safeGetRequest("oj");
$pid = $con->safeGetRequest("pid");
$op = $con->safeGetRequest("op");
//echo "dlsahdkasdgaskd<br>";

if($pid != 0 && $oj!='') {
    $query = "select * from v_problems where oj = '$oj' and problem_id = '$pid'";
    $result = $con->exeSql($query);
    $rowId = 0;
    if($row=mysqli_fetch_array($result,MYSQLI_BOTH)) {
        $rowId = $row["id"];
        $problem = new \crawler\crawler_common\problemBaseInfo();
        $problem->description = $row['description'];
        $problem->input = $row['input'];
        $problem->sampleInput = $row['sample_input'];
        $problem->output = $row['output'];
        $problem->sampleOutput = $row['sample_output'];
        $problem->hint = $row['hint'];
        $problem->timeLimit = $row['time_limit'];
        $problem->memoryLimit = $row['memory_limit'];
        $problem->title = $row['title'];
        $problem->scanf64 = $row['scanf64'];
        $problem->source = $row['source'];
        //$problem->id = $row['problem_id'];
        $problem->complete = true;
        //returnPageError("咦？！");
    }
    if($op=="UPD5725") { //传入秘密参数
        if ($oj == "hdu") {
            $problem = new crawler\hduProblem($pid);
        } else if($oj=='cf') {
            $problem = new crawler\cfProblem($pid);
        }
        else returnPageError('oj不存在！');

        if ($problem->complete == false) {
            returnPageError('该题暂不可用：' . $problem->error);
        }
        if($rowId==0) {
            $query = "INSERT INTO `v_problems` 
                      (
                        oj,
                        problem_id,
                        title,
                        time_limit,
                        memory_limit,
                        scanf64,
                        description,
                        input,
                        output,
                        sample_input,
                        sample_output,
                        hint,
                        source,
                        update_time
                      ) VALUES 
                      (
                        '" . $oj . "',
                        '" . $pid . "',
                        '" . addslashes($problem->title) . "',
                        '" . addslashes($problem->timeLimit) . "',
                        '" . addslashes($problem->memoryLimit) . "',
                        '" . addslashes($problem->scanf64) . "',
                        '" . addslashes($problem->description) . "',
                        '" . addslashes($problem->input) . "',
                        '" . addslashes($problem->output) . "',
                        '" . addslashes($problem->sampleInput) . "',
                        '" . addslashes($problem->sampleOutput) . "',
                        '" . addslashes($problem->hint) . "',
                        '" . addslashes($problem->source) . "',
                        '" . date('Y-m-d H:i:s') . "'
                      );
                      ";
        }
        else {
            $query = "UPDATE `v_problems` SET
                            oj = '" . $oj . "',
                            problem_id = '" . $pid . "',
                            title =  '" . addslashes($problem->title) . "',
                            time_limit = '" . addslashes($problem->timeLimit) . "',
                            memory_limit = '" . addslashes($problem->memoryLimit) . "',
                            scanf64 = '" . addslashes($problem->scanf64) . "',
                            description =  '" . addslashes($problem->description) . "',
                            input = '" . addslashes($problem->input) . "',
                            output = '" . addslashes($problem->output) . "',
                            sample_input = '" . addslashes($problem->sampleInput) . "',
                            sample_output = '" . addslashes($problem->sampleOutput) . "',
                            hint = '" . addslashes($problem->hint) . "',
                            source = '" . addslashes($problem->source) . "',
                            update_time = '" . date('Y-m-d H:i:s') . "'
                      where id = $rowId
                      ";
        }
        $con->exeSql($query);
    }
    else if($rowId==0) {
        returnPageError('题目不存在，或者系统未能及时抓取！');
    }
} else returnPageError('参数错误');

?>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({
    extensions: ["tex2jax.js"],
    jax: ["input/TeX","output/HTML-CSS"],
    tex2jax: {inlineMath: [["$","$"],["\\(","\\)"]]}
  });
</script>
<script src="common/Script/MathJax-master/MathJax.js" type="text/javascript"></script>
    <script language="JavaScript">
        $(function () {
            $('#ProblemLi').addClass('nav-current');
            var oj = <?php echo "'".$oj."'"?>;
            var pid = <?php echo "'".$pid."'"?>;

            $('#problemSubmit').click(function () {
                $('#showSubmitModel').modal('toggle');
            });

            $('#submitReset').click(function () {
                $('#submitCode').val("");
                $('#submitLanguage').val("1");
            });

            $('#submitSubmit').click(function () {
                var code = $('#submitCode').val();
                if(code == '') {
                    return;
                }
                $.post('post/submitWritePost.php', {oj: oj,pid: pid,code: code,language: $('#submitLanguage').val()}, function (data1) {
                    $('#showSubmitModel').modal('toggle');
                    var result1 = JSON.parse(data1);
                    if (result1.status == 1) {
                        $('#AlertP').html(result1.message);
                        $('#ErrorAlert').modal('toggle');
                    } else {
                        $('#AlertP').html(result1.message);
                        $('#ErrorAlert').modal('toggle');
                    }
                });
            });
            var oj = <?="'$oj'" ?>;
            $('#submitLanguage').change(function() {
                document.cookie= oj+"language="+$('#submitLanguage').val(),+";";
            });
        });
    </script>
<div class="container">
    <h1 class="text-center text-primary"><?=$problem->title ?></h1>
    <div class="row">
        <div class="col-sm-4"><h5>Time Limit: <?=$problem->timeLimit ?></h5></div>
        <div class="col-sm-4"><h5>Memory Limit: <?=$problem->memoryLimit ?></h5></div>
        <div class="col-sm-4"><h5>64bit IO Format: <?=$problem->scanf64 ?></h5></div>
    </div>
    <hr />
    <div class="row-margin-bottom">
        <button class="btn btn-info bn-sm col-sm-offset-4" id="problemSubmit">submit</button>
        <button class="btn btn-warning bn-sm col-sm-offset-2">status</button>
    </div>
    <h3 class="text-primary">Description</h3>
    <div class="panel panel-primary block-pre">
        <div class="panel-body"><?=$problem->description ?> </div>
    </div>

    <h3 class="text-primary">Input</h3>
    <div class="panel panel-primary block-pre">
        <div class="panel-body bg-gray"><?=$problem->input ?></div>
    </div>

    <h3 class="text-primary">Output</h3>
    <div class="panel panel-primary block-pre">
        <div class="panel-body bg-gray"><?=$problem->output ?></div>
    </div>

    <h3 class="text-primary">Sample Input</h3>
    <div class="panel panel-primary block-pre">
        <div class="panel-body bg-gray"><?=$problem->sampleInput ?></div>
    </div>

    <h3 class="text-primary">Sample Output</h3>
    <div class="panel panel-primary block-pre">
        <div class="panel-body bg-gray"><?=$problem->sampleOutput ?></div>
    </div>

    <h3 class="text-primary">Hint</h3>
    <div class="panel panel-primary block-pre">
        <div class="panel-body bg-gray"><?=$problem->hint ?></div>
    </div>
</div>
    <div class="modal fade" id="showSubmitModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false">
        <div class="modal-dialog" style="width: 50%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h3 class="modal-title text-danger" id="myModalLabel">
                        Problem - <?=$oj.' '.$pid?>
                    </h3>
                </div>
                <div class="modal-body">
                            <!-- div class="row">
                                <div class="col-sm-3 text-center"><h4>OJ：<span id="submitOjSpan"></span></h4></div>
                                <div class="col-sm-6 text-center"><h4>Problem：<span id="submitProblemSpan"></span></h4></div>
                            </div !-->
                            <div class="row row-margin-bottom">
                                <label class="col-xs-2 col-xs-offset-1 text-right text-primary" for="submitLanguage">Language：</label>
                                <div class="col-xs-6">
                                    <select class="form-control" id='submitLanguage'>
                                    <?php
                                        $query = "select * from v_language where oj='$oj'";
                                        $result = $con->exeSql($query);
                                        $lan = isset($_COOKIE[$oj."language"])?$_COOKIE[$oj."language"]:"";
                                        while($row=mysqli_fetch_array($result,MYSQLI_BOTH)) {
                                            if($lan==$row["value"]) $sel="selected='true'";
                                            else $sel="";
                                            echo "<option value='".$row["value"]."' $sel>".$row["show"]."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row row-margin-bottom">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <textarea title="submitCode" class="form-control" rows="25" id="submitCode"></textarea>
                                </div>
                            </div>
                            <hr/>
                            <div class="row text-center">
                                <div class="col-sm-2 col-sm-offset-4">
                                    <input type="button" class="btn btn-primary btn-block" value="submit" id="submitSubmit">
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-default btn-block" id="showSubmitClose" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
include "common/footer.php";