<?php
/**
 * Created by PhpStorm.
 * User: 15545
 * Date: 2016/7/29
 * Time: 14:59
 */
include 'common/common.php';
include_once 'common/config.php';

include 'common/filter.php';
include 'common/head.php';
include 'common/navigation.php';

?>
<script language="JavaScript">
    $(function () {
        $('#HomePageLi').addClass('nav-current');
    })
</script>
<div class="container">
    <div class="row jumbotron">
        <div class="row text-center">
                <p class="text-success" style="font-size: 50px;">Welcome to NEFU Virtual Judge!</p>
                <br>
                <p class="text-danger container-row-color-content2">Tip: This site is not compatible with IE6, IE7, IE8, recommend the use of Chrome browser.</p>
        </div>
        <hr/>
        <p class="text-warning text-center container-row-color-content2">This Virtual Judge currently supports the following online judges</p>
        <div class="row text-center">
            HDU(杭州电子科技大学OJ), <a href="http://acm.hdu.edu.cn/">http://acm.hdu.edu.cn/</a>
        </div>
        <div class="row text-center">
            Codeforces, <a href="http://www.codeforces.com/">http://www.codeforces.com/</a>
        </div>
    </div>
    <div class="row jumbotron">
        <div class="row text-center">
            <p class="container-row-color-content2">You can also visit our <a href="http://acm.nefu.edu.cn/">real online judge</a></p>
        </div>
    </div>
</div>
<?php
include "common/footer.php";
