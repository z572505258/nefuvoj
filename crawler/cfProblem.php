<?php
/**
 * Created by PhpStorm.
 * User: 15545
 * Date: 2016/7/29
 * Time: 15:03
 */
namespace crawler;
require_once 'common/problemBaseInfo.php';
require_once 'common/tools.php';
use crawler\crawler_common\tools;

class cfProblem extends crawler_common\problemBaseInfo
{
    ///$html例如传进来整个description，$add页面原地址，返回新的description, $root保存图片相对于data的目录，例如传入$root='/hdu/hdu/'
    private function picturePro($html) {
        $rooturl = 'http://codeforces.com/';
        $dataroot = 'fjnL7jiPyT/'; //为了不透露oj名称，此处就表示cf的文件夹

        $pattern = '/\<img .*?src=.*?[ \>]/';
        if(preg_match($pattern,$html,$res)) {
            $imgTag = $res[0];
            $space = false;
            if($imgTag[strlen($imgTag)-1]==' ') $space = true;
            $imgTag[strlen($imgTag)-1] = '>';
            $picAdd =  getValue($imgTag,'src=','>'); //获取图片地址
            $picAdd = preg_replace('/[\'\"]/','',$picAdd); //去掉单引号和双引号
            while(preg_match('/^http:\/\/codeforces.com\//',$picAdd)) { //去掉前缀
                $picAdd = preg_replace('/^http:\/\/codeforces.com\//','',$picAdd);
            }
            while(preg_match('/^..\//',$picAdd)) { //去掉../
                $picAdd = preg_replace('/^..\//','',$picAdd);
            }

            ///保存图片
            $img = file_get_contents($rooturl . $picAdd);

            $p = '/.*\//';
            preg_match($p,APP_ROOT.'/../data/'.$dataroot . $picAdd,$res);
            if(!is_dir($res[0])) mkdir($res[0],0777,true);
            file_put_contents(APP_ROOT.'/../data/'.$dataroot . $picAdd, $img);
            //return $picAdd;
            if($space) $space = ' ';
            else $space = '>';
            $replacement = preg_replace('/src=.*?>/',      'src=data/'.$dataroot . $picAdd.$space ,       $imgTag );
            //return $replacement;
            return preg_replace($pattern, $replacement, $html);
        }
        else return $html;
    }

    private $problemUrl = "http://codeforces.com/problemset/problem/%s/%s";
    private $charset = "utf-8";

    public function __construct($problemId)
    {
        $this->error = "题目暂不可用";
        $this->scanf64 = "%I64d OR %lld";


        $pattern="/[0-9]*/";
        if (preg_match($pattern,$problemId,$res))  $pidA = $res[0]; //pid数字部分
        else return;

        $pattern="/[A-Z]/";
        if (preg_match($pattern,$problemId,$res))  $pidB = $res[0]; //pid字母部分，字母只能有一个
        else return;

        if($pidA.$pidB!=$problemId) { //判断pid输入是否合法
            $this->error = "参数pid不合法".$pidA.$pidB;
            return;
        }
        $this->problemUrl = sprintf(
            $this->problemUrl,
            $pidA,
            $pidB
        );
        $html = getHtml($this->problemUrl, $this->charset);

        $pattern="/\<title\>Codeforces\<\/title\>/";
        if (preg_match($pattern,$html)) {
            $this->error = "题目不存在";
            return;
        }

        $this->problemId = $problemId;
        $this->rawUrl = $this->problemUrl;

        $pattern = "/\<div class=\"title\"\>.*?\<\/div\>/";
        if (preg_match($pattern,$html,$res)) $this->title = substr($res[0], 22 , strlen($res[0])-28);

        $pattern = "/\<div class=\"time-limit\"\>\<div class=\"property-title\"\>time limit per test\<\/div\>.*?\<\/div\>/";
        if (preg_match($pattern,$html,$res)) $this->timeLimit = substr($res[0], 77 , strlen($res[0])-83);

        $pattern = "/\<div class=\"memory-limit\"\>\<div class=\"property-title\"\>memory limit per test\<\/div\>.*?\<\/div\>/";
        if (preg_match($pattern,$html,$res)) $this->memoryLimit = substr($res[0], 81 , strlen($res[0])-87);

        $inputFile = "";
        $pattern = "/\<div class=\"property-title\"\>input\<\/div\>.*?\<\/div\>/";
        if (preg_match($pattern,$html,$res)) $inputFile = substr($res[0], 39 , strlen($res[0])-45);
        if($inputFile=="standard input") $inputFile = "";
        else $inputFile = "Input File: <span style='color:red;font-weight: bold;'>".$inputFile."</span><br/>";

        $outputFile = "";
        $pattern = "/\<div class=\"property-title\"\>output\<\/div\>.*?\<\/div\>/";
        if (preg_match($pattern,$html,$res)) $outputFile = substr($res[0], 40 , strlen($res[0])-46);
        $rawOutput = addslashes($outputFile);
        if($outputFile=="standard output") $outputFile = "";
        else $outputFile = "Output File: <span style='color:red;font-weight: bold'>".$outputFile."</span><br/><br/>";

        $pattern = "/\<div class=\"output-file\"\>\<div class=\"property-title\"\>output\<\/div\>".$rawOutput."\<\/div\>\<\/div\>\<div\>[\s\S]*?\<\/div\>\<div class=\"input-specification\"\>/";
        $this->description = $inputFile.$outputFile."<span class='text-danger'>No Description</span>";
        if (preg_match($pattern,$html,$res)) {
            $this->description = $inputFile.$outputFile.substr($res[0], 82+strlen($rawOutput) , strlen($res[0])-strlen($rawOutput)-121);
            $this->description = $this->picturePro($this->description);
        }
        else {
            $pattern = "/\<div class=\"output-file\"\>\<div class=\"property-title\"\>output\<\/div\>".$rawOutput."\<\/div\>\<\/div\>\<div\>[\s\S]*?\<\/div\>\<div\>\<div class=\"section-title\"\>/";
            preg_match($pattern,$html,$res);
            $this->description = $inputFile.$outputFile.substr($res[0], 82+strlen($rawOutput) , strlen($res[0])-strlen($rawOutput)-120);
            $this->description = $this->picturePro($this->description);
        }


        $pattern = "/\<div class=\"section-title\"\>Input\<\/div\>.*?\<\/div\>\<div class=\"output-specification\"\>/";
        if (preg_match($pattern,$html,$res)) {
            $this->input = substr($res[0], 38 , strlen($res[0])-78);
            $this->input = $this->picturePro($this->input);
        }
        else {
            $pattern = "/\<div class=\"section-title\"\>Interaction\<\/div\>.*?\<\/div\>\<div class=\"sample-tests\"\>/";
            preg_match($pattern,$html,$res);
            $this->input = substr($res[0], 44 , strlen($res[0])-76);
            $this->input = $this->picturePro($this->input);
            $this->output = "<span class='text-danger'>Please view the input above.</span>";
        }


        $pattern = "/\<div class=\"section-title\"\>Output\<\/div\>.*?\<\/div\>\<div class=\"sample-tests\"\>/";
        if (preg_match($pattern,$html,$res)) {
            $this->output = substr($res[0], 39 , strlen($res[0])-71);
            $this->output = $this->picturePro($this->output);
        }

        $pattern = "/\<div class=\"sample-tests\"\>\<div class=\"section-title\"\>Examples\<\/div><div class=\"sample-test\"\>.*?\<\/pre\>\<\/div\>\<\/div\>\<\/div\>/";
        if (preg_match($pattern,$html,$res,0,1)) {
            $this->sampleInput = substr($res[0], 67 , strlen($res[0])-73);
            $this->sampleInput = $this->picturePro($this->sampleInput);
        }
        else {
            $pattern = "/\<div class=\"sample-tests\"\>\<div class=\"section-title\"\>Example\<\/div><div class=\"sample-test\"\>.*?\<\/pre\>\<\/div\>\<\/div\>\<\/div\>/";
            preg_match($pattern,$html,$res,0,1);
            $this->sampleInput = substr($res[0], 66 , strlen($res[0])-72);
            $this->sampleInput = $this->picturePro($this->sampleInput);
        }

        $this->sampleOutput = "<span class='text-danger'>Please view the Sample Input above.</span>";
        $pattern = "/\<div class=\"section-title\"\>Note\<\/div\>.*?\<\/p\>\<\/div\>/";
        if (preg_match($pattern,$html,$res)) {
            $this->hint = substr($res[0], 37 , strlen($res[0])-43);
            $this->hint = $this->picturePro($this->hint);
        }

        //$pattern = "/<i style='font-size:1px'> </i>[\s\S]*?/";
        //if (preg_match($pattern,$html,$res)) $this->sampleOutput = substr($res[0], 139 , strlen($res[0])-151);

        $this->complete = true;
    }
}