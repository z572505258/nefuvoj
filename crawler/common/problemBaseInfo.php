<?php
/**
 * Created by PhpStorm.
 * User: 15545
 * Date: 2016/7/29
 * Time: 15:33
 */

namespace crawler\crawler_common;


class problemBaseInfo
{
    public $complete = false;
    public $error = "";

    public $id=0;
    public $problemId="";
    public $title="";
    public $timeLimit="";
    public $memoryLimit="";
    public $scanf64="";
    public $description="";
    public $input="";
    public $output="";
    public $sampleInput="";
    public $sampleOutput="";
    public $hint="";
    public $source="";

    public $rawUrl="";


    public function printJson() {
        $problemInfo = array();
        $problemInfo["problemId"] = $this->problemId;
        $problemInfo["title"] = $this->title;
        $problemInfo["timelimit"] = $this->timelimit;
        $problemInfo["memoryLimit"] = $this->memoryLimit;
        $problemInfo["description"] = $this->description;
        $problemInfo["input"] = $this->input;
        $problemInfo["output"] = $this->output;
        $problemInfo["sampleInput"] = $this->sampleInput;
        $problemInfo["sampleOutput"] = $this->sampleOutput;
        $problemInfo["hint"] = $this->hint;
        $problemInfo["source"] = $this->source;
        $problemInfo["rawUrl"] = $this->rawUrl;

        echo json_encode($problemInfo);
    }
}