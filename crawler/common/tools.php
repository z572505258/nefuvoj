<?php
function getHtml($problemUrl , $rawCharset) {
    @$fh= file_get_contents($problemUrl);
    if($fh==null) {
        returnPageError("tools getHtml : 网络错误，无法连接。");
        exit();
    }
    if($rawCharset!='utf-8')
        $fh = mb_convert_encoding($fh, 'utf-8',$rawCharset);
    return $fh;
}
function getValue($html,$leftS,$rightS) {
    $leftLen = strlen($leftS);
    $leftPos = strpos($html,$leftS);
    $rightPos = strpos($html,$rightS);
    //echo $leftPos+$leftLen;echo " ";echo $rightPos-$leftPos-$leftLen."<br>";
    //if($leftPos==false || $rightPos==false) return "";
    return substr($html,$leftPos+$leftLen , $rightPos-$leftPos-$leftLen);
}



