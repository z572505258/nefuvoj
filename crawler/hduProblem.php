<?php
/**
 * Created by PhpStorm.
 * User: 15545
 * Date: 2016/7/29
 * Time: 15:03
 */
namespace crawler;
require_once 'common/problemBaseInfo.php';
require_once 'common/tools.php';

class hduProblem extends crawler_common\problemBaseInfo
{
    ///$html例如传进来整个description，$add页面原地址，返回新的description, $root保存图片相对于data的目录，例如传入$root='/hdu/hdu/'
    private function picturePro($html, $rooturl ,$dataroot) {
        $pattern = '/\<img .*?src=.*?\>/';
        if(preg_match($pattern,$html,$res)) {
            $imgTag = $res[0];
            $picAdd =  getValue($imgTag,'src=','>'); //获取图片地址
            $picAdd = preg_replace('/[\'\"]/','',$picAdd);
            while(preg_match('/^..\//',$picAdd)) {
                $picAdd = preg_replace('/^..\//','',$picAdd);
            }
            ///保存图片
            $img = file_get_contents($rooturl . $picAdd);
            file_put_contents(APP_ROOT.'/../data/'.$dataroot . $picAdd, $img);
            //return $picAdd;
            $replacement = preg_replace('/src=.*?>/',      'src=data/'.$dataroot . $picAdd.'>' ,       $imgTag );
            //return $replacement;
            return preg_replace($pattern, $replacement, $html);
        }
        else return $html;
    }

    private $problemUrl = "http://acm.hdu.edu.cn/showproblem.php?pid=%s";
    private $charset = "gbk";

    public function __construct($problemId)
    {
        $this->error = "题目暂不可用";
        $rooturl = 'http://acm.hdu.edu.cn/';
        $dataroot = 'UjmhsUYuyT/'; //为了不透露oj名称，此处就表示hdu的文件夹
        $this->scanf64 = "%I64d OR %lld";

        $this->problemUrl = sprintf(
            $this->problemUrl,
            $problemId
        );
        $html = getHtml($this->problemUrl, $this->charset);

        $pattern="/<TD><img src=\"\/images\/msg.png\"/";
        if (preg_match($pattern,$html)) {
            $this->error = "题目不存在";
            return;
        }

        $this->problemId = $problemId;
        $this->rawUrl = $this->problemUrl;

        $pattern = "/\<h1 style=\'color:#1A5CC8\'\>.*?\<\/h1\>/";
        if (preg_match($pattern,$html,$res)) $this->title = substr($res[0], 26 , strlen($res[0])-31);

        $pattern = "/bold\;color\:green\'\>Time Limit: [0-9\/]* MS \(Java\/Others\)/";
        if (preg_match($pattern,$html,$res)) $this->timeLimit = substr($res[0], 30 , 100);

        $pattern = "/Memory Limit: [0-9\/]* K \(Java\/Others\)/";
        if (preg_match($pattern,$html,$res)) $this->memoryLimit = substr($res[0], 14 , 100);

        $pattern = "/\<div class=panel_content\>[\s\S]*?\<\/div\>\<div class=panel_bottom\>\&nbsp\;\<\/div\>/";
        if (preg_match($pattern,$html,$res)) {
            $this->description = substr($res[0], 25 , strlen($res[0])-67);
            $this->description = $this->picturePro($this->description, $rooturl ,$dataroot);
        }

        $pattern = "/\<div class=panel_title align=left\>Input\<\/div\> \<div class=panel_content\>[\s\S]*?\<\/div\>\<div class=panel_bottom\>&nbsp;\<\/div\>/";
        if (preg_match($pattern,$html,$res)) {
            $this->input = substr($res[0], 71 , strlen($res[0])-113);
            $this->input = $this->picturePro($this->input, $rooturl ,$dataroot);
        }

        $pattern = "/\<div class=panel_title align=left\>Output\<\/div\> \<div class=panel_content\>[\s\S]*?\<\/div\>\<div class=panel_bottom\>&nbsp;\<\/div\>/";
        if (preg_match($pattern,$html,$res)) {
            $this->output = substr($res[0], 72 , strlen($res[0])-114);
            $this->output = $this->picturePro($this->output, $rooturl ,$dataroot);
        }

        $pattern = "/\<div class=panel_title align=left\>Sample Input\<\/div\>\<div class=panel_content\>\<pre\>\<div style=\"font-family\:Courier New\,Courier,monospace\;\"\>[\s\S]*?\<\/div\>\<\/pre\>/";
        if (preg_match($pattern,$html,$res)) {
            $this->sampleInput = '<pre>'.substr($res[0], 138 , strlen($res[0])-150) .'</pre>';
            $this->sampleInput = $this->picturePro($this->sampleInput, $rooturl ,$dataroot);
        }

        $pattern = "/\<div class=panel_title align=left\>Sample Output\<\/div\>\<div class=panel_content\>\<pre\>\<div style=\"font-family\:Courier New\,Courier,monospace\;\"\>[\s\S]*?\<\/div\>\<\/pre\>/";
        if (preg_match($pattern,$html,$res)) {
            $this->sampleOutput = '<pre>'.substr($res[0], 139 , strlen($res[0])-151) .'</pre>';
            $this->sampleOutput = $this->picturePro($this->sampleOutput, $rooturl ,$dataroot);
        }

        //$pattern = "/<i style='font-size:1px'> </i>[\s\S]*?/";
        //if (preg_match($pattern,$html,$res)) $this->sampleOutput = substr($res[0], 139 , strlen($res[0])-151);

        $this->complete = true;
    }
}