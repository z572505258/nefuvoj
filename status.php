<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/2/17
 * Time: 9:48
 */
include 'common/common.php';
include_once 'common/config.php';

include 'common/filter.php';
include 'common/head.php';
include 'common/navigation.php';
?>

    <script language="JavaScript">
        $(function () {
            $('#StatusLi').addClass('nav-current');
        })
    </script>

    <script>
        $(function () {
            $('#status-list-table').bootstrapTable({
                //height: $(window).height() - $('.main-header').outerHeight(true) - $('.main-navigation').outerHeight(true) - $('#status-search-div').outerHeight(true),
                //height: pageSize,
                classes: 'table table-striped table-condensed table-hover',
                method: 'get',
                url: 'post/statusListPost.php',
                //showRefresh: true,
                //search: false,
                columns: [
                    {
                        field: 'solution_id',
                        title: 'Run ID',
                        align: 'center',
                        width: '6%'
                    }, {
                        field: 'user_name',
                        title: 'User',
                        align: 'center',
                        width: '6%'
                    }, {
                        field: 'nick_name',
                        title: 'Nick Name',
                        align: 'center',
                        width: '6%'
                    }, {
                        field: 'problem_id',
                        title: 'Problem',
                        align: 'center',
                        width: '8%',
                        formatter: function (value, row, index) {
                            return "<a href='problemShow.php?problem_id=" + value + "' target='_blank'>" + value + "</a>";
                        }
                    }, {
                        field: 'result',
                        title: 'Result',
                        align: 'center',
                        width: '18%'
                    },{
                        field: 'language',
                        title: 'Language',
                        align: 'center',
                        width: '10%'
                    }, {
                        field: 'memory',
                        title: 'Memory',
                        align: 'center',
                        width: '6%'
                    }, {
                        field: 'time',
                        title: 'Time',
                        align: 'center',
                        width: '6%'
                    }, {
                        field: 'code_length',
                        title: 'Code Length',
                        align: 'center',
                        width: '7%'
                    }, {
                        field: 'in_date',
                        title: 'Submit Time',
                        align: 'center',
                        width: '10%'
                    }],
                pagination: true,
                sidePagination: 'server',
                pageSize: 25,
                queryParams: function (params) {
                    return {
                        limit: params.limit,
                        offset: params.offset,
                        pid: $('#search-pid').val(),
                        uid: $('#search-uid').val(),
                        result: $('#search-result').val(),
                        language: $('#search-language').val(),
                        params: params
                    };
                }
            });
            $('#search-go').click(function () {
                var table = $('#status-list-table');
                table.bootstrapTable('refresh');
            });
            setInterval(function(){
                $('#status-list-table').bootstrapTable('refresh');
            },60000);
        });

        function showCE(id) {
            $.post('post/statusCEPost.php', {solution_id: id}, function (data) {
                var result = JSON.parse(data);
                if(result.status==1){
                    $('#CEProblem').html(result.data.Problem);
                    $('#CEAuthor').html(result.data.Author);
                    $('#CEJudgeStatus').html(result.data.JudgeStatus);
                    $('#CEInformation').html("<xmp>" + result.data.CEInformation + "</xmp>");
                    $('#showCEModel').modal('toggle');
                }
                else{
                    $('#AlertP').html(result.message);
                    $('#ErrorAlert').modal('toggle');
                }
            });
        }
        function showSC(id) {
            $.post('post/statusSCPost.php', {solution_id: id}, function (data) {
                var result = JSON.parse(data);
                if(result.status==1) {
                    $('#SCProblem').html(result.data.Problem);
                    $('#SCAuthor').html(result.data.Author);
                    $('#SCJudgeStatus').html(result.data.JudgeStatus);
                    $('#SCCode').html("<xmp class=\"prettyprint\">" + result.data.SCCode + "</xmp>");
                    prettyPrint();
                    $('#showSCModel').modal('toggle');
                }else{
                    $('#AlertP').html(result.message);
                    $('#ErrorAlert').modal('toggle');
                }

            });
        }
    </script>
    <div class="modal fade" id="showSCModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 900px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h3 class="modal-title" id="myModalLabel">
                        Source Code
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <label class="col-sm-3 col-sm-offset-1">Problem：<span id="SCProblem"></span></label>
                                <label class="col-sm-3">Author：<span id="SCAuthor"></span></label>
                                <label class="col-sm-5">Judge Status：<span id="SCJudgeStatus"></span></label>
                            </div>
                        </div>
                        <div class="panel-body">
                            <pre id="SCCode"></pre>
                        </div>
                        <div class="panel-heading hidden" id="SCCEShow">
                            <pre id="SCCEInformation" style="color:red"></pre>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="showSCClose" data-dismiss="modal">Close
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" id="showCEModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 900px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h3 class="modal-title" id="myModalLabel">
                        Error Log
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <label class="col-sm-3 col-sm-offset-1">Problem：<span id="CEProblem"></span></label>
                                <label class="col-sm-3">Author：<span id="CEAuthor"></span></label>
                                <label class="col-sm-5">Judge Status：<span id="CEJudgeStatus"></span></label>
                            </div>
                        </div>
                        <div class="panel-body">
                            <pre id="CEInformation" style="color:red"></pre>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="showCEClose" data-dismiss="modal">Close
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="col-sm-10 col-sm-offset-1" id="status-list" style="margin-top: 2em">
        <!-- div-- class="row row-margin-bottom" id="status-search-div">
            <div class="col-sm-2">
                <div class="row">
                    <label for="search-pid" class="col-xs-5">Problem：</label>
                    <div class="col-xs-6">
                        <!-- ?php
                        $search_pid = isset($_REQUEST['problem']) ? $_REQUEST['problem'] :0;
                        if($search_pid!=0){
                            echo "<input type='text' class='form-control' id='search-pid' value='".$search_pid."'>";
                        }
                        else
                        {
                            echo "<input type='text' class='form-control' id='search-pid'>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="row">
                    <label for="search-uid" class="col-xs-2">User：</label>
                    <div class="col-xs-9">
                        <!-- ?php
                        $search_uid = isset($_REQUEST['user']) ? $_REQUEST['user'] :"";
                        if($search_uid!=""){
                            echo "<input type='text' class='form-control' id='search-uid' value='".$search_uid."'>";
                        }
                        else
                        {
                            echo "<input type='text' class='form-control' id='search-uid'>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="row">
                    <label for='search-result' class="col-xs-3">Result：</label>
                    <div class="col-xs-9">
                        <select id='search-result' class="form-control">
                            <option value="0" selected>All</option>
                            <option value="1">Accepted</option>
                            <option value="2">Presentation Error</option>
                            <option value="3">Time Limit Exceeded</option>
                            <option value="4">Memory Limit Exceeded</option>
                            <option value="5">Wrong Answer</option>
                            <option value="6">Runtime Error</option>
                            <option value="7">Output Limit Exceeded</option>
                            <option value="8">Compile Error</option>
                            <option value="13">Floating Point Error</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="row">
                    <label for='search-language' class="col-xs-4">Language：</label>
                    <div class="col-xs-8">
                        <select id='search-language' class="form-control">
                            <option value='0'>All</option>
                            <option value='1'>C++ (g++ 3.4.3)</option>
                            <option value='2'>C (gcc 3.4.3)</option>
                            <option value='3'>JAVA (jdk 1.5.0)</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                <input type="button" class='form-control' value="Go" id="search-go">
            </div>
        </div !-->
        <div class="row row-margin-bottom" id="status-search-div">
            <div class="col-sm-4">
                <div class="row">
                    <label for="search-pid" class="col-xs-3 text-right">Problem：</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="search-pid" placeholder="like 'hdu1000'">
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <label for="search-uid" class="col-xs-2 text-right">User：</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="search-uid" placeholder="username...">
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="row">
                    <label for="search-result" class="col-xs-3 text-right">Result：</label>
                    <div class="col-xs-9">
                        <select id="search-result" class="form-control">
                            <option value="" selected="">All</option>
                            <option value="Accepted">Accepted</option>
                            <option value="Presentation">Presentation Error</option>
                            <option value="Time">Time Limit Exceeded</option>
                            <option value="Memory">Memory Limit Exceeded</option>
                            <option value="Wrong">Wrong Answer</option>
                            <option value="Runtime">Runtime Error</option>
                            <option value="Output">Output Limit Exceeded</option !-->
                            <option value="Comp">Compile Error</option>
                            <option value="Submit Failed">Submit Failed</option>
                        </select>
                    </div>
                </div>
            </div>
            <!--div class="col-sm-3">
                <div class="row">
                    <label for="search-language" class="col-xs-4">Language：</label>
                    <div class="col-xs-8">
                        <select id="search-language" class="form-control">
                            <option value="0">All</option>
                            <option value="1">C++ (g++ 3.4.3)</option>
                            <option value="2">C (gcc 3.4.3)</option>
                            <option value="3">JAVA (jdk 1.5.0)</option>
                        </select>
                    </div>
                </div>
            </div!-->
            <div class="col-sm-1">
                <input type="button" class="form-control btn btn-success" value="Filter" id="search-go">
            </div>
        </div>
        <table id="status-list-table">
        </table>
    </div>
<?php
include 'common/footer.php';